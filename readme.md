Fuzzy Dice
==

A dice-rolling system where the dice faces have success, critical success, and
blank sides rather than numbers. The result of a roll is determined by the
number of success and critical success faces showing after the dice are rolled.
Fantasy Flight Games' tabletop games are examples of this kind of dice system.  

Running the Example
--
The webpage in the `example` folder provides a demo. Run `npm install` to
download dependencies, then `npm run example` to compile the Javascript for the
demo. Visit `example/index.html` in a browser to view the demo. The example's
Javascript at `example/page_scripts.js` demonstrates how to integrate Fuzzy
Dice into your own project.

NPM Scripts
--
`npm run prepublish` - Compiles the library and places the output in `lib/fuzzy-dice.min.js`
`npm run example` - Builds the demo. Open `example/index.html` in a browser after this command has been run to see the demo.
`npm run watch` - Watches `fuzzy-dice` source files for changes and rebuilds the example automatically when changes are detected.
`npm run test` - Builds the tests and runs them.  
`npm run build-tests` - Builds but doesn't run the tests.  

The API
--

Fuzzy Dice exports the following constants that each represent one of the
possible outcomes of a roll:

`FAILURE`: failure is defined as a roll in which all the dice rolled showed
blank sides  
`PARTIAL_SUCCESS`: partial success is defined as a roll in which at least one
of the dice rolled showed a success or a critical success face, but not enough
successes or critical successes were rolled to beat the DC or the opposed roll  
`SUCCESS`: success is defined as a roll in which the dice showed enough success
and critical success faces to meet or exceed the DC or the opposed roll  
`CRITICAL_SUCCESS`: a critical success is a roll on which the number of
critical success faces met or exceeded the critical success threshold set for
the dice; this outcome supersedes all other results if it occurs.  


The `Dice` object

This object captures the properties of the dice you want to roll.
Each of Fuzzy Dice's API functions will require a Dice object be passed to it.

properties:
* num_sides: the number of sides on each die, must be a positive integer  
* num_blank_sides: the number of sides on each die that correspond to an
  unsuccessful result, must be a positive integer and less than or equal to
  the die_sides parameter  
* num_crit_sides: the number of sides on each die that correspond to a
  special critical roll result, must be a positive integer less than or
  equal to the die_sides parameter or zero  
* crit_threshold: the number of critical results that must be rolled for a roll
  to be automatically counted as a critical success, must be a positive
  integer, or null to disable automatic crits (note that this property is
  ignored for opposed dice since opposed dice cannot crit)  
* generator: the function used to generate a random floating point value
  between 0 (inclusive) and 1 (exclusive); this parameter is useful to
  ensure deterministic values when testing or if you simply want to use a
  different random number generator then the default; defaults to
  Math.random  

example:
```
import * as FuzzyDice from 'fuzzy-dice';

// a six-sided die with three blank faces, two success faces, and one critical
// success face
let num_sides = 6;
let num_blank_sides = 3;
let num_crit_sides = 1;
let crit_threshold = null;
let dice = new FuzzyDice.Dice(
	num_sides, num_blank_sides, num_crit_sides, crit_threshold
);

// a die that uses a custom random number generator, in this case a generator
// that always outputs the highest possible roll
let generator = () => 0.9;
let loaded_dice = new FuzzyDice.Dice(
	num_sides, num_blank_sides, num_crit_sides, crit_threshold, generator
);
```


`roll(dice, num_dice)`

Rolls the given number of dice and returns the number of success and critical
success results.

parameters:
* dice: a Dice object giving the properties of the dice to use
* num_dice: the number of dice to roll, must be a positive integer or 0

returns an object with the following properties:
```
{
    num_successes: 2,          // the number of successes rolled
    num_criticals: 0,          // the number of crits rolled
}
```


`check_vs_dc(dice_type, num_dice, dc)`

Rolls the given number of dice and returns the results against the given
Difficulty Class, which represents the threshold number of success and critical
success faces that must appear for the roll to be a success.

parameters:
* dice_type: a Dice object giving the properties of the dice to use
* num_dice: the number of dice to roll, must be a positive integer or 0
* dc: a positive integer or 0 giving the number of success or critical success
  rolls needed to achieve overall success

returns an object with the following properties:
```
{
    num_successes: 2,          // the number of successes rolled

    num_criticals: 0,          // the number of crits rolled

    dc: 3,                     // the DC being tested against

    outcome: PARTIAL_SUCCESS,  // one of the FuzzyDice results constants giving
                               // the overall result for this check

    magnitude: -1              // the number of successes by which the check
                               // succeeded or failed, or null for failures and
                               // critical successes since these are absolute
}
```


`opposed_check(dice_type, num_dice, opposed_dice_type, num_opposed_dice)`

Rolls the given number of dice and returns the results against the given
opposed dice. Critical success sides on opposed dice will be treated as normal
success sides.

parameters:
* dice_type: a Dice object giving the properties of the dice to use
* num_dice: the number of dice to roll, must be a positive integer or 0
* opposed_dice_type: a Dice object giving the properties of the opposed dice;
  note that opposed dice are not allowed to crit so their critical success
  sides will be treated as normal success sides for this check
* num_opposed_dice: the number of opposed dice to roll, must be a positive
  integer or 0

returns an object with the following properties:
```
{
    num_successes: 2,          // the number of successes rolled

    num_criticals: 0,          // the number of crits rolled

    num_opposed_successes: 1,  // the number of successes in the opposed roll

    outcome: SUCCESS,          // one of the Fuzzy Dice results constants giving
                               // the overall result for this check

    magnitude: 1               // the number of successes by which the check
                               // succeeded or failed, or null for failures and
                               // critical successessince these are absolute
}
```


`probabilities_vs_dc(dice_type, num_player_dice, dc)`

Calculates the probability distribution for the given check vs DC.

parameters:
* dice_type: a Dice object giving the properties of the dice to use
* num_player_dice: the number of dice to roll, must be a positive integer or 0
* dc: a positive integer or 0 giving the number of success or critical success
  rolls needed to achieve overall success

returns an object with the probability of each outcome, with the corresponding
result constant as the key:
```
{
    FuzzyDice.CRITICAL_SUCCESS: 0.135,
    FuzzyDice.SUCCESS: 0.347,
    FuzzyDice.PARTIAL_SUCCESS: 0.220,
    FuzzyDice.FAILURE: 0.298
}
```


`probabilities_vs_opposed_roll(
	dice_type, num_player_dice, opposed_dice_type, num_opposed_dice
)`

Calculates the probability distribution for the given check vs an opposed roll.

parameters:
* dice_type: a Dice object giving the properties of the dice to use
* num_player_dice: the number of dice to roll, must be a positive integer or 0
* opposed_dice_type: a Dice object giving the properties of the opposed dice
* num_opposed_dice: the number of opposed dice to roll, must be a positive
  integer or 0

returns an object with the probability of each outcome, with the corresponding
result constant as the key:
```
{
    FuzzyDice.CRITICAL_SUCCESS: 0.135,
    FuzzyDice.SUCCESS: 0.347,
    FuzzyDice.PARTIAL_SUCCESS: 0.220,
    FuzzyDice.FAILURE: 0.298
}
```
