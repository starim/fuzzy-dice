import * as FuzzyDice from '../src/fuzzy-dice.js';

// set the icon to use for successes in the display
const COUNTER_SYMBOL = "•";
const CRIT_SYMBOL = "★";


// This is the logic for handling the actual rolls using Fuzzy Dice.
function roll() {
  let player_dice = new FuzzyDice.Dice(
    num_sides(), num_blanks(), num_criticals(), crit_threshold()
  );

  let result;
  if (roll_type() === "dc") {
    result = FuzzyDice.check_vs_dc(player_dice, num_dice(), dc());

    let dc_string = "";
    for (let counter = 0; counter < result.dc; counter++) {
      dc_string += COUNTER_SYMBOL;
    }
    document.querySelector(".dc-count").textContent = dc_string;
    document.querySelector(".dc-count-label").textContent = "DC";
  } else {
    let opposed_dice = new FuzzyDice.Dice(
      num_sides(), num_blanks(), 0, null
    );

    result = FuzzyDice.opposed_check(
      player_dice, num_dice(), opposed_dice, num_opposed_dice()
    );

    let opposed_roll_string = "";
    for (let counter = 0; counter < result.num_opposed_successes; counter++) {
      opposed_roll_string += COUNTER_SYMBOL;
    }
    document.querySelector(".dc-count").textContent = opposed_roll_string;
    document.querySelector(".dc-count-label").textContent = "Opposed Roll";
  }

  document.querySelectorAll(".result").forEach(tag => tag.style.display = "block");
  document.querySelectorAll(".result-summary").forEach(tag => tag.style.display = "none");
  document.querySelectorAll(".result-text").forEach(tag => tag.style.display = "none");


  let success_string = "";
  for (let counter = 0; counter < result.num_successes; counter++) {
    success_string += COUNTER_SYMBOL;
  }
  for (let counter = 0; counter < result.num_criticals; counter++) {
    success_string += CRIT_SYMBOL;
  }
  document.querySelector(".success-count").textContent = success_string;

  let magnitude_string;
  if (result.magnitude === null) {
    if (result.outcome === FuzzyDice.CRITICAL_SUCCESS) {
      magnitude_string = "N/A (critical success is absolute)";
    } else {
      magnitude_string = "N/A (failure is absolute)";
    }
  } else {
    magnitude_string = result.magnitude;
  }
  document.querySelector(".result-magnitude").textContent = magnitude_string;

  switch (result.outcome) {
    case FuzzyDice.CRITICAL_SUCCESS:
      document.querySelector(".result-summary-crit").style.display = "block";
      document.querySelector(".result-text-crit").style.display = "block";
      break;
    case FuzzyDice.SUCCESS:
      document.querySelector(".result-summary-success").style.display = "block";
      document.querySelector(".result-text-success").style.display = "block";
      break;
    case FuzzyDice.PARTIAL_SUCCESS:
      document.querySelector(".result-summary-partial-success").style.display = "block";
      document.querySelector(".result-text-partial-success").style.display = "block";
      break;
    case FuzzyDice.FAILURE:
      document.querySelector(".result-summary-failure").style.display = "block";
      document.querySelector(".result-text-failure").style.display = "block";
      break;
  }
}

// This is the logic for fetching the probability distribution for a given
// roll, if you want to display to a user what their odds are before they
// actually roll.
function calculate_odds() {
  let player_dice = new FuzzyDice.Dice(
    num_sides(), num_blanks(), num_criticals(), crit_threshold()
  );

  let odds;
  if (roll_type() === "dc") {
    odds = FuzzyDice.probabilities_vs_dc(player_dice, num_dice(), dc());
  } else {
    let opposed_dice = new FuzzyDice.Dice(
      num_sides(), num_blanks(), 0, null
    );
    odds = FuzzyDice.probabilities_vs_opposed_roll(
      player_dice,
      num_dice(),
      opposed_dice,
      num_opposed_dice()
    );
  }

  let percent = (probability) => {
    return `${Math.round(probability * 100)}%`;
  }
  document.querySelector(".critical-success-probability").textContent =
    percent(odds[FuzzyDice.CRITICAL_SUCCESS]);
  document.querySelector(".success-probability").textContent =
    percent(odds[FuzzyDice.SUCCESS]);
  document.querySelector(".partial-success-probability").textContent =
    percent(odds[FuzzyDice.PARTIAL_SUCCESS]);
  document.querySelector(".failure-probability").textContent =
    percent(odds[FuzzyDice.FAILURE]);
}


// private functions
//

// gets the currently set number of sides for dice
function num_sides() {
  return parseInt(document.querySelector("#num-sides-btn").value);
}

// gets the currently set number of blank sides on each dice
function num_blanks() {
  return parseInt(document.querySelector("#num-blanks-btn").value);
}

// gets the currently set number of critical sides on each dice
function num_criticals() {
  return parseInt(document.querySelector("#num-criticals-btn").value);
}

// gets the currently set number of player dice to roll
function num_dice() {
  return parseInt(document.querySelector(".input-num-dice").value);
}

// gets the currently set roll type, either "dc" or "opposed-dice"
function roll_type() {
  return document.querySelector("input[name=roll-type]:checked").value;
}

// gets the currently set DC
function dc() {
  return parseInt(document.querySelector(".input-dc").value);
}

// gets the currently set number of opposed dice to roll
function num_opposed_dice() {
  return parseInt(document.querySelector(".input-opposed-dice").value);
}

// gets the currently set number of opposed dice to roll
function crit_threshold() {
  let crit_threshold = parseInt(document.querySelector("#crit-threshold-btn").value);
  if (crit_threshold <= 0) {
    return null;
  } else {
    return crit_threshold;
  }
}


document.addEventListener("DOMContentLoaded", () => {
  document.querySelector(".roll-btn").addEventListener("click", roll);


  // show/hide page elements depending on which roll type the user wants
  let update_roll_type = () => {
    let types = ["dc", "opposed-dice"];
    let current_choice = roll_type();
    console.log(`roll type is now set to ${current_choice}`);
    for (let class_name of types) {
      if (current_choice === class_name) {
        document.querySelector(`.${class_name}`).style.display = "block";
      } else {
        document.querySelector(`.${class_name}`).style.display = "none";
      }
    }
  }
  document.querySelector(".roll-type").addEventListener("change", update_roll_type);
  update_roll_type(); // init

  document.querySelectorAll(
    ".dice-properties input, .roll-options input, .roll-type-option input"
  ).forEach(tag => tag.addEventListener("change", calculate_odds));

  calculate_odds(); // init
});
