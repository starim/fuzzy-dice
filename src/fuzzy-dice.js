"use strict";

// the result of a check will be expressed in one of these constants
const FAILURE = "failure";
const PARTIAL_SUCCESS = "partial success";
const SUCCESS = "success";
const CRITICAL_SUCCESS = "critical success";


// The properties of the dice to roll when this library computes rolls.
//
// num_sides: the number of sides on each die, must be a positive integer
// num_blank_sides: the number of sides on each die that correspond to an
//   unsuccessful result, must be a positive integer and less than or equal to
//   the die_sides parameter
// num_crit_sides: the number of sides on each die that correspond to a
//   special critical roll result, must be a positive integer less than or
//   equal to the die_sides parameter or zero
// crit_threshold: the number of critical results that must be rolled for a roll
//   to be automatically counted as a critical success, must be a positive
//   integer or null to disable automatic crits (note that this property is
//   ignored for opposed dice as opposed dice cannot crit)
// generator: the function used to generate a random floating point value
//   between 0 (inclusive) and 1 (exclusive); this parameter is useful to
//   ensure deterministic values when testing or if you simply want to use a
//   different random number generator then the default; defaults to
//   Math.random
class Dice {
  constructor(
    num_sides, num_blank_sides, num_crit_sides, crit_threshold,
    generator = Math.random,
  ) {
    this.num_sides = num_sides;
    this.num_blank_sides = num_blank_sides;
    this.num_crit_sides = num_crit_sides;
    this.crit_threshold = crit_threshold;
    this.generator = generator;
  }
};


// Rolls the given number of dice and returns the results against the given DC.
//
// dice_type: a Dice object giving the properties of the dice to use
// num_dice: the number of dice to roll, must be a positive integer or 0
// dc: a positive integer or 0 giving the number of success rolls needed to
//   achieve overall success
//
// returns an object with the following keys:
//   num_successes: the number of successes rolled
//   num_criticals: the number of crits rolled
//   dc: the dc being tested against
//   num_dice: the number of dice that were used in the roll
//   outcome: a value from the enum at the top of this file indicating the
//     overall result of the roll (e.g. FAILURE or PARTIAL_SUCCESS)
//   magnitude: the number of successes by which the roll succeeded or failed
//     (e.g. 5 if the roll beat the DC by 5, or -4 if the roll failed the DC by
//     4), or null for critical successes or failures since these are absolute
function check_vs_dc(dice_type, num_dice, dc) {
  let outcome = roll(dice_type, num_dice);
  let computed_outcome = result(
    outcome.num_successes,
    outcome.num_criticals,
    dc,
    dice_type.crit_threshold
  );
  return {
    num_successes: outcome.num_successes,
    num_criticals: outcome.num_criticals,
    num_dice: outcome.num_dice,
    dc: dc,
    outcome: computed_outcome.outcome,
    magnitude: computed_outcome.magnitude
  };
}

// Rolls the given number of dice and returns the results against the given
// opposed dice.
//
// dice_type: a Dice object giving the properties of the dice to use
// num_dice: the number of dice to roll, must be a positive integer or 0
// opposed_dice_type: a Dice object giving the properties of the opposed dice
// num_opposed_dice: the number of opposed dice to roll, must be a positive
//   integer or 0
//
// returns an object with the following keys:
//   num_successes: the number of successes rolled
//   num_criticals: the number of crits rolled
//   num_opposed_successes: the number of successes in the opposed roll
//   num_dice: the number of dice that were used in the roll
//   num_dice: the number of dice that were oppossed in the roll
//   outcome: a value from the enum at the top of this file indicating the
//     overall result of the roll (e.g. FAILURE or PARTIAL_SUCCESS)
//   magnitude: the number of successes by which the roll succeeded or failed
//     (e.g. 5 if the roll beat the DC by 5, or -4 if the roll failed the DC by
//     4), or null for critical successes or failures since these are absolute
function opposed_check(
  dice_type, num_dice, opposed_dice_type, num_opposed_dice,
) {
  let outcome = roll(dice_type, num_dice);
  let opposed_outcome = roll(opposed_dice_type, num_opposed_dice);
  let dc = opposed_outcome.num_successes + opposed_outcome.num_criticals;
  let computed_outcome = result(
    outcome.num_successes,
    outcome.num_criticals,
    dc,
    dice_type.crit_threshold
  );
  return {
    num_successes: outcome.num_successes,
    num_criticals: outcome.num_criticals,
    num_opposed_successes: dc,
    num_dice: num_dice,
    num_opposed_dice: num_opposed_dice,
    outcome: computed_outcome.outcome,
    magnitude: computed_outcome.magnitude
  };
}

// Rolls the given number of dice and returns the number of success results.
//
// dice: a Dice object giving the properties of the dice to use
// num_dice: the number of dice to roll, must be a positive integer or 0
//
// returns an object with the following keys:
//   num_successes: the number of successes rolled
//   num_criticals: the number of crits rolled
function roll(dice, num_dice) {
  let successes = 0;
  let criticals = 0;
  for (let counter = 1; counter <= num_dice; counter++) {
    let side_rolled = Math.floor(dice.generator() * dice.num_sides) + 1;
    console.log(`Rolled a ${side_rolled} on ${dice.num_sides}-sided die #${counter}...`);
    if (side_rolled > dice.num_blank_sides) {
      if (
        dice.num_crit_sides &&
        (side_rolled > (dice.num_sides - dice.num_crit_sides))
      ) {
        criticals++;
        console.log("\t...which is a crit");
      } else {
        successes++;
        console.log("\t...which is a success");
      }
    } else {
      console.log("\t...which is a blank");
    }
  }

  return {
    num_criticals: criticals,
    num_successes: successes
  };
}

// Calculates the binomial coefficient of a and b.
function binomial_coefficient(a, b) {
  if (b > a && a > 0 && b > 0) {
    return 0.0;
  };
  return (factorial(a) / (factorial(a - b) * factorial(b) * 1.0));
}

// Calculates the factorial of n.
//
// n: the number whose factorial will be computed, must be a positive integer
//   or zero
//
// returns an integer value or NaN if n is less than zero.
function factorial(n) {
  if (n < 0) {
    return NaN;
  }
  if (n == 0 || n == 1) {
    return 1;
  }

  let factorial = n;
  for (let i = (n - 1); i > 1; --i) {
    factorial *= i;
  }
  return factorial;
}

// Calculates the probability distribution for the given check vs DC.
//
// dice_type: a Dice object giving the properties of the dice to use
// num_player_dice: the number of dice to roll, must be a positive integer or 0
// dc: a positive integer or 0 giving the number of success rolls needed to
//   achieve overall success
//
// Returns an object with keys for each result type, each giving the corresponding
// probability. For example:
//
// {
//   FuzzyDice.CRITICAL_SUCCESS: 0.135,
//   FuzzyDice.SUCCESS: 0.347,
//   FuzzyDice.PARTIAL_SUCCESS: 0.220,
//   FuzzyDice.FAILURE: 0.298
// }
function probabilities_vs_dc(dice_type, num_player_dice, dc) {
  let probabilities = {};
  probabilities[CRITICAL_SUCCESS] = probability_crit(dice_type, num_player_dice);
  probabilities[SUCCESS] = probability_success(dice_type, num_player_dice, dc);
  probabilities[FAILURE] =
    probability_failure(dice_type, num_player_dice, dc);
  probabilities[PARTIAL_SUCCESS] =
    1.0 -
    probabilities[CRITICAL_SUCCESS] -
    probabilities[SUCCESS] -
    probabilities[FAILURE];

  return probabilities;
}

// Calculates the probability distribution for the given check vs an opposed roll.
//
// dice_type: a Dice object giving the properties of the dice to use
// num_player_dice: the number of dice to roll, must be a positive integer or 0
// opposed_dice_type: a Dice object giving the properties of the opposed dice
// num_opposed_dice: the number of opposed dice to roll, must be a positive
//   integer or 0
//
// Returns an object with keys for each result type, each giving the corresponding
// probability. For example:
//
// {
//   FuzzyDice.CRITICAL_SUCCESS: 0.135,
//   FuzzyDice.SUCCESS: 0.347,
//   FuzzyDice.PARTIAL_SUCCESS: 0.220,
//   FuzzyDice.FAILURE: 0.298
// }
function probabilities_vs_opposed_roll(
  dice_type, num_player_dice, opposed_dice_type, num_opposed_dice,
) {
  // opposed rolls can't crit, so turn crit sides into success sides for the
  // purposes of this function
  opposed_dice_type = JSON.parse(JSON.stringify(opposed_dice_type));
  opposed_dice_type.num_crit_sides = 0;

  let probabilities = {};
  probabilities[CRITICAL_SUCCESS] = 0.0;
  probabilities[SUCCESS] = 0.0;
  probabilities[PARTIAL_SUCCESS] = 0.0;
  probabilities[FAILURE] = 0.0;

  let dc_probabilities =
    probabilities_for_opposed_roll(opposed_dice_type, num_opposed_dice);
  // calculate the probability of each result vs a fixed DC, weighted by the
  // probability of that DC being the opposed roll result
  for (let dc in dc_probabilities) {
    let probability_of_this_dc = dc_probabilities[dc];
    let probabilities_vs_this_dc = probabilities_vs_dc(dice_type, num_player_dice, dc);
    for (let result_type in probabilities_vs_this_dc) {
      probabilities[result_type] +=
        (probability_of_this_dc * probabilities_vs_this_dc[result_type]);
    }
  }

  return probabilities;
}


// private functions
//

// Computes the overall result of a check given the number of successes and
// crits rolled and a DC.
//
// num_successes: a positive integer or 0 giving the number of successes rolled
// num_criticals: a positive integer or 0 giving the number of crits rolled
// dc: an integer giving the number of successes required for a check to be a
//   complete success
// crit_threshold: a positive integer giving the number of crits required to
//   turn a roll into an automatic critical success
//
// returns an object with the following properties:
//   outcome: a value from the enum at the top of this file indicating the
//     overall result of the roll (e.g. FAILURE or PARTIAL_SUCCESS)
//   magnitude: the number of successes by which the roll succeeded or failed
//     (e.g. 5 if the roll beat the DC by 5, or -4 if the roll failed the DC by
//     4), or null for critical successes or failures since these are absolute
function result(
  num_successes,
  num_criticals,
  dc,
  crit_threshold,
) {
  let total_successes = num_successes + num_criticals;
  let outcome;
  if (crit_threshold && num_criticals >= crit_threshold) {
    outcome = {
      outcome: CRITICAL_SUCCESS,
      magnitude: null
    };
  } else if (total_successes >= dc) {
    outcome = {
      outcome: SUCCESS,
      magnitude: total_successes - dc
    };
  } else if (total_successes > 0) {
    outcome = {
      outcome: PARTIAL_SUCCESS,
      magnitude: total_successes - dc
    };
  } else {
    outcome = {
      outcome: FAILURE,
      magnitude: null
    };
  }

  return outcome;
}

// Calculates the probability of rolling a critical success.
//
// dice: a Dice object giving the properties of the dice to use
// No: the number of dice to roll, must be a positive integer or 0
//
// returns a floating point number giving a probability value between 0.0 and 1.0
function probability_crit(dice, No) {
  const Nc = dice.crit_threshold;
  const So = dice.num_sides;
  const Sc = dice.num_crit_sides;

  if (Nc == null || Nc > No) {
    return 0.0;
  };

  let probability = 0.0;
  for (let i = Nc; i <= No; i++) {
    probability += (
      Math.pow(Sc / (So * 1.0), i) *
      Math.pow((So - Sc) / (So * 1.0), No - i) *
      binomial_coefficient(No, i)
    );
  }

  return probability;
}

// Calculates the probability of rolling a success.
//
// dice: a Dice object giving the properties of the dice to use
// No: the number of dice to roll, must be a positive integer or 0
// dc: a positive integer or 0 giving the number of success rolls needed to
//   achieve overall success
//
// returns a floating point number giving a probability value between 0.0 and 1.0
function probability_success(dice, No, dc) {
  const Nc = dice.crit_threshold;
  const So = dice.num_sides;
  const Sg = dice.num_sides - dice.num_blank_sides - dice.num_crit_sides;
  const Sb = dice.num_blank_sides;
  const Sc = dice.num_crit_sides;

  if (dc > No) {
    return 0.0;
  }
  if (dc == 0) {
    return (1 - probability_crit(dice, No, Nc));
  }

  let probability = 0.0;
  for (let i = dc; i <= No; i++) {
    probability += (
      Math.pow((Sg + Sc) / (So * 1.0), i) *
      Math.pow(Sb / (So * 1.0), No - i) *
      binomial_coefficient(No, i)
    );
  }

  if (Nc != null && Nc <= No) {
    let Pc_overlap = 0.0;
    for (let j = Math.max(dc, Nc); j <= No; j++) {
      Pc_overlap += (
        Math.pow(Sc / (So * 1.0), j) *
        Math.pow((So - Sc) / (So * 1.0), No - j) *
        binomial_coefficient(No, j)
      );
    }

    probability -= Pc_overlap;
  }

  return probability;
}

// Calculates the probability of rolling a failure.
//
// dice: a Dice object giving the properties of the dice to use
// No: the number of dice to roll, must be a positive integer or 0
// dc: a positive integer or 0 giving the number of success rolls needed to
//   achieve overall success
//
// returns a floating point number giving a probability value between 0.0 and 1.0
function probability_failure(dice, No, dc) {
  const So = dice.num_sides;
  const Sb = dice.num_blank_sides;

  if (dc == 0) {
    return 0.0;
  }

  return Math.pow(Sb / (So * 1.0), No);
}

// Calculates the probability distribution for rolling the given opposed die
// the given number of times.
//
// dice: a Dice object giving the properties of the dice to use
// num_dice: the number of times to roll the dice, must be a positive integer
//   or zero
function probabilities_for_opposed_roll(dice, num_dice) {
  // probability of rolling a success on a single roll
  let Ps = (dice.num_sides - dice.num_blank_sides) / (dice.num_sides * 1.0);
  // probability of rolling a blank on a single roll
  let Pb = (dice.num_blank_sides) / (dice.num_sides * 1.0);
  let probabilities = [];
  for (let i = 0; i <= num_dice; i++) {
    probabilities[i] =
      Math.pow(Ps, i) *
      Math.pow(Pb, num_dice - i) *
      binomial_coefficient(num_dice, i);
  }

  return probabilities;
}

export {
  FAILURE,
  PARTIAL_SUCCESS,
  SUCCESS,
  CRITICAL_SUCCESS,
  Dice,
  check_vs_dc,
  opposed_check,
  roll,
  probabilities_vs_dc,
  probabilities_vs_opposed_roll,
  /* develblock:start */
  binomial_coefficient, 
  factorial,
  /* develblock:end */
}
