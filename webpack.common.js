const path = require('path');

module.exports = {
  mode: "production",
  entry: ['./src/fuzzy-dice.js'],
  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: 'fuzzy-dice.min.js',
    library: "FuzzyDice",
  },
  module: {
    rules: [{
      test: /\.m?js$/,
      exclude: /(node_modules)/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      },{
        loader: 'webpack-strip-block'
      }]
    }, {
      test: /\.scss/,
      exclude: /(node_modules)/,
      use: ['css-loader', 'sass-loader']
    }, {
      test: /\.css/,
      exclude: /(node_modules)/,
      use: 'css-loader'
    }]
  },
  resolve: {
    extensions: ['.js', '.scss']
  },
  plugins: []
};
