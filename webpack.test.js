const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: "development",
  devtool: false,
  entry: ['./test/setup/index.js'],
  output: {
    path: path.resolve(__dirname, 'test'),
    filename: 'test-bundle.js'
  },
  module: {
    rules: [{
      test: /\.m?js$/,
      exclude: /(node_modules)/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }]
    }]
  },
  resolve: {
    extensions: ['.js']
  },
});
