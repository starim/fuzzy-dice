const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: "source-map",
  entry: ['./example/page_scripts.js'],
  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: 'example-fuzzy-dice.js'
  },
  watchOptions: {
    ignored: /node_modules/
  }
});
