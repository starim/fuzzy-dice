export function assert_equal_float(expected, actual, epsilon) {
  if (Math.abs(expected - actual) > epsilon) {
    throw (new Error(
      `Equality assertion failed:
      expected: (${typeof(expected)}) ${expected}
      actual: (${typeof(actual)}) ${actual}
      difference: ${expected - actual}
      `
    ));
  }
  return true;
}

export function one_die_normal_distribution_generator(die_sides) {
  let roll_number = 0;
  return () => {
    if (roll_number >= die_sides) {
      throw (
        `roll generator exhausted--the test is trying to make too many rolls`
      );
    }
    let this_roll = roll_number;
    roll_number++;
    return (this_roll * 1.0) / die_sides;
  };
}

export function two_dice_normal_distribution_generator(die_sides) {
  let first_die_roll = 0;
  let second_die_roll = 0;
  let first_die_rolled = false;
  return () => {
    if (first_die_roll >= die_sides) {
      throw (
        `roll generator exhausted--the test is trying to make too many rolls`
      );
    }
    let this_roll;
    if (!first_die_rolled) {
      this_roll = first_die_roll;
      first_die_rolled = true;
    } else {
      this_roll = second_die_roll;
      first_die_rolled = false;
      if (second_die_roll >= (die_sides - 1)) {
        second_die_roll = 0;
        first_die_roll++;
      } else {
        second_die_roll++;
      };
    }
    return (this_roll * 1.0) / die_sides;
  };
}

export function alternating_crit_generator() {
  let alternator = true;
  let crit_alternator = true;
  return () => {
    let roll;
    if (alternator) {
      roll = 0.0;
    } else {
      if (crit_alternator) {
        roll = 0.99;
      } else {
        roll = 0.5;
      }
      crit_alternator = !crit_alternator;
    }
    alternator = !alternator;
    return roll;
  };
}
