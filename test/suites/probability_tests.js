import * as FuzzyDice from '../../src/fuzzy-dice';
import {
  assert_equal_float
} from '../setup/lib_test';
import chai from 'chai';
const expect = chai.expect;

describe('FuzzyDice Probability Tests', () => {

  describe('factorial() ', () => {
    it('should return NaN for negative inputs', () => {
      expect(isNaN(FuzzyDice.factorial(-1))).to.equal(true);
      expect(isNaN(FuzzyDice.factorial(-200))).to.equal(true);
    });
    it('should return 1 for input values of 0 and 1', () => {
      expect(FuzzyDice.factorial(0)).to.equal(1);
      expect(FuzzyDice.factorial(1)).to.equal(1);
    });
    it('should return 40,320 for an input value of 8', () => {
      expect(FuzzyDice.factorial(8)).to.equal(40320);
    });
  });

  describe('binomial_coefficient() ', () => {
    it('should return mathematically correct outputs', () => {
      expect(isNaN(FuzzyDice.binomial_coefficient(-5.0, -3.0))).to.equal(true);
      expect(isNaN(FuzzyDice.binomial_coefficient(-5.0, 3.0))).to.equal(true);
      expect(isNaN(FuzzyDice.binomial_coefficient(5.0, -3.0))).to.equal(true);
      expect(FuzzyDice.binomial_coefficient(4.0, 5.0)).to.equal(0.0);
      expect(FuzzyDice.binomial_coefficient(1.0, 0.0)).to.equal(1.0);
      expect(FuzzyDice.binomial_coefficient(3.0, 2.0)).to.equal(3.0);
      expect(FuzzyDice.binomial_coefficient(5.0, 3.0)).to.equal(10.0);
    });
  });

  describe('probabilities_vs_dc() ', () => {
    describe(`with the following properties:
      DC = 0
      number of player dice = 0
      crit threshold = 2
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 0;
      let player_dice = 0;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 2;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);


      it('should return expected results', () => {
        expect(probability[FuzzyDice.CRITICAL_SUCCESS].toFixed(3)).to.equal("0.000");
        expect(assert_equal_float(1.000, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      });
    });

    describe(`with the following properties:
      DC = 0
      number of player dice = 3
      crit threshold = 2
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 0;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 2;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);

      it('should return expected results', () => {
        expect(assert_equal_float(0.259, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.741, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      });
    });

    describe(`with the following properties:
      DC = 1
      number of player dice = 3
      crit threshold = 2
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 1;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 2;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);

      it('should return expected results', () => {
        expect(assert_equal_float(0.259, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.736, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.005, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      });
    });

    describe(`with the following properties:
      DC = 2
      number of player dice = 3
      crit threshold = 2
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 2;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 2;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);

      it('should return expected results', () => {
        expect(assert_equal_float(0.259, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.667, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.069, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.005, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      });
    });

    describe(`with the following properties:
      DC = 3
      number of player dice = 3
      crit threshold = 2
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 3;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 2;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);
      it('should return expected results', () => {
        expect(assert_equal_float(0.259, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.542, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.194, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.005, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      })
    });

    describe(`with the following properties:
      DC = 4
      number of player dice = 3
      crit threshold = 2
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 4;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 2;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);
      it('should return expected results', () => {
        expect(assert_equal_float(0.259, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.736, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.005, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      })
    });

    describe(`with the following properties:
      DC = 0
      number of player dice = 3
      crit threshold = 3
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 0;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 3;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);
      it('should return expected results', () => {
        expect(assert_equal_float(0.037, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.963, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      })
    });

    describe(`with the following properties:
      DC = 1
      number of player dice = 3
      crit threshold = 3
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 1;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 3;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);
      it('should return expected results', () => {
        expect(assert_equal_float(0.037, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.958, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.005, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      })
    });

    describe(`with the following properties:
      DC = 2
      number of player dice = 3
      crit threshold = 3
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 2;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 3;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);
      it('should return expected results', () => {
        expect(assert_equal_float(0.037, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.889, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.069, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.005, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      })
    });

    describe(`with the following properties:
      DC = 3
      number of player dice = 3
      crit threshold = 3
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 3;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 3;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);
      it('should return expected results', () => {
        expect(assert_equal_float(0.037, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.542, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.417, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.005, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      })
    });

    describe(`with the following properties:
      DC = 4
      number of player dice = 3
      crit threshold = 3
      die sides = 6
      success die sides = 3
      crit die sides = 2`, () => {

      let dc = 4;
      let player_dice = 3;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = 3;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);
      it('should return expected results', () => {
        expect(assert_equal_float(0.037, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.000, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.958, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.005, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      })
    });

    describe(`with the following properties:
      DC = 2
      number of player dice = 2
      crit threshold = null
      die sides = 8
      success die sides = 3
      crit die sides = 1`, () => {

      let dc = 2;
      let player_dice = 2;
      let die_sides = 8;
      let blank_sides = 4;
      let crit_sides = 1;
      let crit_threshold = null;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_dc(die, player_dice, dc);
      it('should return expected results', () => {
        expect(assert_equal_float(0.000, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.250, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.500, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
        expect(assert_equal_float(0.250, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
      })
    });

    describe(`with the following properties:
      number of player dice = 1
      player dice properties:
      crit threshold = 3
      die sides = 6
      success die sides = 3
      crit die sides = 2
      number of opposed dice = 1
      opposed dice properties:
      die sides = 4
      success die sides = 2`, () => {

      let player_dice = 1;
      let die_sides = 6;
      let blank_sides = 1;
      let crit_sides = 2;
      let crit_threshold = null;
      let opposed_dice = 1;
      let opposed_die_sides = 4;
      let opposed_blank_sides = 2;
      let opposed_crit_sides = 0;
      let opposed_crit_threshold = null;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );
      let opposed_die = new FuzzyDice.Dice(
        opposed_die_sides,
        opposed_blank_sides,
        opposed_crit_sides,
        opposed_crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_opposed_roll(
        die, player_dice, opposed_die, opposed_dice
      );
      expect(assert_equal_float(0.000, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
      expect(assert_equal_float(0.917, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
      expect(assert_equal_float(0.000, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
      expect(assert_equal_float(0.083, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
    });

    describe(`with the following properties:
      number of player dice = 3
      player dice properties:
      crit threshold = 2
      die sides = 8
      success die sides = 2
      crit die sides = 1
      number of opposed dice = 4
      opposed dice properties:
      die sides = 5
      success die sides = 3`, () => {

      let player_dice = 3;
      let die_sides = 8;
      let blank_sides = 5;
      let crit_sides = 1;
      let crit_threshold = 2;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );
      let opposed_dice = 2;
      let opposed_die_sides = 5;
      let opposed_blank_sides = 2;
      let opposed_crit_sides = 0;
      let opposed_crit_threshold = null;
      let opposed_die = new FuzzyDice.Dice(
        opposed_die_sides,
        opposed_blank_sides,
        opposed_crit_sides,
        opposed_crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_opposed_roll(
        die, player_dice, opposed_die, opposed_dice
      );
      expect(assert_equal_float(0.043, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
      expect(assert_equal_float(0.594, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
      expect(assert_equal_float(0.158, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
      expect(assert_equal_float(0.205, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
    });
  });


  describe('probabilities_vs_opposed_roll()', () => {
    it(`shouldn't give opposed dice a chance to crit`, () => {
      let player_dice = 3;
      let die_sides = 8;
      let blank_sides = 5;
      let crit_sides = 1;
      let crit_threshold = 2;
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold
      );
      let opposed_dice = 2;
      let opposed_die_sides = 5;
      let opposed_blank_sides = 2;
      let opposed_crit_sides = 3;
      let opposed_crit_threshold = 1;
      let opposed_die = new FuzzyDice.Dice(
        opposed_die_sides,
        opposed_blank_sides,
        opposed_crit_sides,
        opposed_crit_threshold
      );

      let probability = FuzzyDice.probabilities_vs_opposed_roll(
        die, player_dice, opposed_die, opposed_dice
      );
      expect(assert_equal_float(0.043, probability[FuzzyDice.CRITICAL_SUCCESS], 0.001)).to.equal(true);
      expect(assert_equal_float(0.594, probability[FuzzyDice.SUCCESS], 0.001)).to.equal(true);
      expect(assert_equal_float(0.158, probability[FuzzyDice.PARTIAL_SUCCESS], 0.001)).to.equal(true);
      expect(assert_equal_float(0.205, probability[FuzzyDice.FAILURE], 0.001)).to.equal(true);
    });
  });
});
