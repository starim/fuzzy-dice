import * as FuzzyDice from '../../src/fuzzy-dice';
import {
  one_die_normal_distribution_generator,
  two_dice_normal_distribution_generator,
  alternating_crit_generator
} from '../setup/lib_test';
import chai from 'chai';
const expect = chai.expect;

describe(`FuzzyDice Probability Tests`, () => {

  describe(`roll()`, () => {

    it(`should produce the right ratio of successes, crits, and \
    blanks for a single die`, () => {
      let die_sides = 6;
      let blank_sides = 3;
      let crit_sides = 1;
      let crit_threshold = null;
      let die = new FuzzyDice.Dice(
        die_sides,
        blank_sides,
        crit_sides,
        crit_threshold,
        one_die_normal_distribution_generator(die_sides)
      );

      let num_successes = 0;
      let num_criticals = 0;
      for (let i = 0; i < die_sides; i++) {
        let result = FuzzyDice.roll(die, 1);
        num_successes += result.num_successes;
        num_criticals += result.num_criticals;
      }

      expect(num_successes).to.equal(2);
      expect(num_criticals).to.equal(1);
    });

    it(`should produce the right ratio of successes, crits, and \
    blanks for two dice`, () => {
      let die_sides = 6;
      let blank_sides = 3;
      let crit_sides = 1;
      let crit_threshold = null;
      let die = new FuzzyDice.Dice(
        die_sides,
        blank_sides,
        crit_sides,
        crit_threshold,
        two_dice_normal_distribution_generator(die_sides)
      );

      let num_successes = 0;
      let num_criticals = 0;
      for (let i = 0; i < (die_sides * die_sides); i++) {
        let result = FuzzyDice.roll(die, 2);
        num_successes += result.num_successes;
        num_criticals += result.num_criticals;
      }

      expect(num_successes).to.equal(24);
      expect(num_criticals).to.equal(12);
    });

    it(`should recognize an all blank roll as a failure`, () => {
      let die_sides = 6;
      let blank_sides = 3;
      let crit_sides = 1;
      let crit_threshold = null;
      let generator = () => {
        return 0.0;
      };
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold, generator
      );

      let result = FuzzyDice.check_vs_dc(die, 2, 1);
      expect(result.num_successes).to.equal(0);
      expect(result.num_criticals).to.equal(0);
      expect(result.dc).to.equal(1);
      expect(result.outcome).to.equal(FuzzyDice.FAILURE);
      expect(result.magnitude).to.equal(null);
    });

  });

  describe(`check_vs_dc()`, () => {

    it(`should recognize an all blank roll as a failure`, () => {
      let die_sides = 6;
      let blank_sides = 3;
      let crit_sides = 1;
      let crit_threshold = null;
      let generator = () => {
        return 0.0;
      };
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold, generator
      );

      let result = FuzzyDice.check_vs_dc(die, 2, 1);
      expect(result.num_successes).to.equal(0);
      expect(result.num_criticals).to.equal(0);
      expect(result.dc).to.equal(1);
      expect(result.outcome).to.equal(FuzzyDice.FAILURE);
      expect(result.magnitude).to.equal(null);
    });

    it(`should recognize a low roll as a partial success`, () => {
      let die_sides = 6;
      let blank_sides = 3;
      let crit_sides = 1;
      let crit_threshold = null;
      let generator = alternating_crit_generator();
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold, generator
      );

      let result = FuzzyDice.check_vs_dc(die, 2, 2);
      expect(result.num_successes).to.equal(0);
      expect(result.num_criticals).to.equal(1);
      expect(result.dc).to.equal(2);
      expect(result.outcome).to.equal(FuzzyDice.PARTIAL_SUCCESS);
      expect(result.magnitude).to.equal(-1);
    });

    it(`should recognize a high roll as a success`, () => {
      let die_sides = 6;
      let blank_sides = 3;
      let crit_sides = 1;
      let crit_threshold = null;
      let generator = alternating_crit_generator();
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold, generator
      );

      let result = FuzzyDice.check_vs_dc(die, 8, 2);
      expect(result.num_successes).to.equal(2);
      expect(result.num_criticals).to.equal(2);
      expect(result.dc).to.equal(2);
      expect(result.outcome).to.equal(FuzzyDice.SUCCESS);
      expect(result.magnitude).to.equal(2);
    });

    it(`should be able to recognize critical successes`, () => {
      let die_sides = 6;
      let blank_sides = 3;
      let crit_sides = 1;
      let crit_threshold = 1;
      let generator = alternating_crit_generator();
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold, generator
      );

      let result = FuzzyDice.check_vs_dc(die, 4, 100);
      expect(result.num_successes).to.equal(1);
      expect(result.num_criticals).to.equal(1);
      expect(result.dc).to.equal(100);
      expect(result.outcome).to.equal(FuzzyDice.CRITICAL_SUCCESS);
      expect(result.magnitude).to.equal(null);
    });

  });

  describe(`opposed_check()`, () => {

    it(`should recognize an all blank roll vs an all  blank roll as a success`,
      () => {
        let die_sides = 6;
        let blank_sides = 3;
        let crit_sides = 1;
        let crit_threshold = null;
        let generator = () => {
          return 0.0;
        };
        let die = new FuzzyDice.Dice(
          die_sides, blank_sides, crit_sides, crit_threshold, generator
        );
        let opposed_die = new FuzzyDice.Dice(
          die_sides, blank_sides, crit_sides, null, generator
        );

        let result = FuzzyDice.opposed_check(die, 2, opposed_die, 2);
        expect(result.num_successes).to.equal(0);
        expect(result.num_criticals).to.equal(0);
        expect(result.num_opposed_successes).to.equal(0);
        expect(result.outcome).to.equal(FuzzyDice.SUCCESS);
        expect(result.magnitude).to.equal(0);
      });

    it(`should recognize an all blank roll vs a positive  roll as a failure`,
      () => {
        let die_sides = 6;
        let blank_sides = 3;
        let crit_sides = 1;
        let crit_threshold = null;
        let generator = () => {
          return 0.0;
        };
        let opposed_generator = () => {
          return 0.5;
        };
        let die = new FuzzyDice.Dice(
          die_sides, blank_sides, crit_sides, crit_threshold, generator
        );
        let opposed_die = new FuzzyDice.Dice(
          die_sides, blank_sides, crit_sides, null, opposed_generator
        );

        let result = FuzzyDice.opposed_check(die, 2, opposed_die, 3);
        expect(result.num_successes).to.equal(0);
        expect(result.num_criticals).to.equal(0);
        expect(result.num_opposed_successes).to.equal(3);
        expect(result.outcome).to.equal(FuzzyDice.FAILURE);
        expect(result.magnitude).to.equal(null);
      });

    it(`should recognize a low roll vs a higher roll as  a partial success`,
      () => {
        let die_sides = 6;
        let blank_sides = 3;
        let crit_sides = 1;
        let crit_threshold = null;
        let generator = () => {
          return 0.5;
        };
        let die = new FuzzyDice.Dice(
          die_sides, blank_sides, crit_sides, crit_threshold, generator
        );
        let opposed_die = new FuzzyDice.Dice(
          die_sides, blank_sides, crit_sides, null, generator
        );

        let result = FuzzyDice.opposed_check(die, 2, opposed_die, 3);
        expect(result.num_successes).to.equal(2);
        expect(result.num_criticals).to.equal(0);
        expect(result.num_opposed_successes).to.equal(3);
        expect(result.outcome).to.equal(FuzzyDice.PARTIAL_SUCCESS);
        expect(result.magnitude).to.equal(-1);
      });

    it(`should recognize a high roll vs a lower roll as  a success`,
      () => {
        let die_sides = 6;
        let blank_sides = 3;
        let crit_sides = 1;
        let crit_threshold = null;
        let generator = () => {
          return 0.5;
        };
        let die = new FuzzyDice.Dice(
          die_sides, blank_sides, crit_sides, crit_threshold, generator
        );
        let opposed_die = new FuzzyDice.Dice(
          die_sides, blank_sides, crit_sides, null, generator
        );

        let result = FuzzyDice.opposed_check(die, 4, opposed_die, 1);
        expect(result.num_successes).to.equal(4);
        expect(result.num_criticals).to.equal(0);
        expect(result.num_opposed_successes).to.equal(1);
        expect(result.outcome).to.equal(FuzzyDice.SUCCESS);
        expect(result.magnitude).to.equal(3);
      });

    it(`should be able to recognize critical successes`, () => {
      let die_sides = 6;
      let blank_sides = 3;
      let crit_sides = 1;
      let crit_threshold = 4;
      let generator = alternating_crit_generator();
      let opposed_generator = () => {
        return 0.5
      };
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold, generator
      );
      let opposed_die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, null, opposed_generator
      );

      let result = FuzzyDice.opposed_check(die, 16, opposed_die, 100);
      expect(result.num_successes).to.equal(4);
      expect(result.num_criticals).to.equal(4);
      expect(result.num_opposed_successes).to.equal(100);
      expect(result.outcome).to.equal(FuzzyDice.CRITICAL_SUCCESS);
      expect(result.magnitude).to.equal(null);
    });

    it(`should output the number of dice used in the check`, () => {
      let die_sides = 6;
      let blank_sides = 3;
      let crit_sides = 1;
      let crit_threshold = 4;
      let generator = alternating_crit_generator();
      let opposed_generator = () => {
        return 0.5
      };
      let die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, crit_threshold, generator
      );
      let opposed_die = new FuzzyDice.Dice(
        die_sides, blank_sides, crit_sides, null, opposed_generator
      );

      let result = FuzzyDice.opposed_check(die, 10, opposed_die, 100);
      expect(result.num_dice).to.equal(10);
      expect(result.num_opposed_dice).to.equal(100);
    });

  });
});
